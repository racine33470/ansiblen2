from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route("/whatever")
def hello_world2():
    return "<p>Hello, Ansible!</p>"

@app.route("/test")
def hello_world3():
    return "<p>Hello, {{ tower_user_name }} </p>"
